<!DOCTYPE html>
<html>
<head>
	<title><?=$page_title?></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>asset/css/bootstrap.min.css"">
</head>
<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light" style="font-family: Rockwell">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="<?php echo base_url(); ?>Autobuses_controller/index">SISTEMA AUTOBUSES</a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="<?php echo base_url(); ?>control_autobuses_controller/index">CONTROL AUTOBUSES<span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="<?php echo base_url(); ?>Autobuses_controller/cronometro">CRONOMETRO</a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">CONTEO</a>
      </li>
    </ul>

  </div>
</nav>

	<?php $this->load->view('template/header'); ?>
	<?php $this->load->view($view,$data_view); ?>
	<?php $this->load->view('template/footer'); ?>
</body>
</html>