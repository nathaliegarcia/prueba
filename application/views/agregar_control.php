<!DOCTYPE html>
<html>
<head>
	<title></title>
	<script type="text/javascript" src="<?php echo base_url();?>asset/jquery-3.4.1.min.js"></script>

</head>
<body style="background-color: #2196f3">
	<form name="agregar" action="<?php echo base_url();?>Control_autobuses_controller/agregar_control" method="post">
		<div align="center" style="font-family: Rockwell">
			<label>Control de autobuses</label><br>
			<input type="text" id="control"><br>
			<label>Numero de viajeros</label><br>
			<input type="text" id="numero"><br>
			<label>Horarios</label><br>
			<select id="horario">
		    <option>Ingrese su Horario</option>
		    <?php foreach ($horario as $valor): ?>
			<option value="<?php echo $valor->idhorario; ?>"><?php echo $valor->horario ?></option>
		    <?php endforeach ?>
			</select><br><br>
			<input type="button" id="insertar" value="Insertar" class="btn btn-primary">
		</div>
		
	</form>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/main.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>asset/js/agregar_control.js"></script>

</body>
</html>