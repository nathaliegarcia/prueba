<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="background-color: #2196f3 ">
<div align="center" class="container" style="font-family: Rockwell">
	<strong><h1>Control de autobuses</h1></strong>
	<table class="table table-bordered">
		<tr>
			<td>Control autobuses</td>
			<td>Control_autobuses</td>
			<td>Numero de viajeros</td>
			<td>Horarios</td>
			<td>Eliminar</td>
			<td>Actualizar</td>
		</tr>
		<?php foreach ($control as $valor): ?>
			<tr>
				<td><?=$valor->idcontrol_autobuses?></td>
				<td><?=$valor->control_autobuses?></td>
				<td><?=$valor->n_viajeros?></td>
				<td><?=$valor->horario?></td>
				<td><a href="<?php echo base_url().'Control_autobuses_controller/eliminar/'.$valor->idcontrol_autobuses?>"class="btn btn-success">ELIMINAR</a></td>

				<td><a href="<?php echo base_url();?>Control_autobuses_controller/update_control" class="btn btn-success">ACTUALIZAR</a></td>
			</tr>
		<?php endforeach ?>
	</table>
	<a href="<?php echo base_url(); ?>control_autobuses_controller/control" class="btn btn-success">Agregar</a>
</div>

</body>
</html>