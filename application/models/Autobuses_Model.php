<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Autobuses_Model extends CI_model{


public function autobuses(){
	$this->db->select('a.idautobuses,a.capacidad,c.caracteristica,p.nombre_calle,h.horario,m.mantenimiento,a.combustible');
	$this->db->from('autobuses as a');
	$this->db->join('caracteristicas as c','c.idcaracteristicas = a.idcaracteristicas','left');
	$this->db->join('horario as h','h.idhorario = a.idhorario','left');
	$this->db->join('mantenimiento as m','m.idmantenimiento = a.idmantenimiento','right');
	$this->db->join('parada_autobuses as p','p.idparada_autobuses = a.idparada_autobuses','left');
	$autobuses = $this->db->get();
	return $autobuses->Result();
}

public function caracteristicas(){
	$caracteristicas = $this->db->get('caracteristicas');
	return $caracteristicas->Result();
}

public function parada_autobuses(){
	$parada_autobuses = $this->db->get('parada_autobuses');
	return $parada_autobuses->Result();
}

public function horario(){
	$horario = $this->db->get('horario');
	return $horario->Result();
}
public function mantenimiento(){
	$mantenimiento = $this->db->get('mantenimiento');
	return $mantenimiento->Result();
}

///// agregar:
public function agregar_autobus($data){

	return ($this->db->insert('autobuses',$data)) ? true:false;
}

//delete de autobus:

public function eliminar($idautobuses){
	$this->db->where('idautobuses ='.$idautobuses);
	$this->db->delete('autobuses');
}

//update de autobuses.
public function actualizar($data){
	$this->db->set('capacidad',$data['capacidad']);
	$this->db->set('idcaracteristicas',$data['caracteristicas']);
	$this->db->set('idparada_autobuses',$data['parada_autobuses']);
	$this->db->set('idhhorario',$data['horario']);
	$this->db->set('idmantenimiento',$data['mantenimiento']);
	$this->db->update('autobuses');

}

}
?>