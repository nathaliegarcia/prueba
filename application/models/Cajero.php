<?php defined('BASEPATH') OR exit('No direct script access allowed');


class cajero_model extends CI_model 
{
public function cajero(){
		$cajero=$this->db->get('cajero');
		return $cajero->Result();
	}

	public function Agregar_Cajero(){
		return ($this->load->insert('cajero')) ? true:false;
	}
	public function Obtener_Cajero($Id_Cajero){
	$this->db->select('*');
	$this->db->from('cajero');
	$this->db->where('Id_Cajero ='.$Id_Cajero);
	$Id_Cajero = $this->db->get();
	return ($Id_Cajero->num_rows() ===1) ? $Id_Cajero->row(): false;
}

public function Actualizar_Cajero($data){
	$this->db->where('Id_Cajero', $data['Id_Cajero']);
	return ($this->db->update('cajero',$data)) ? true:false;
}
public function eliminar($Id_Cajero){
 $this->db->where('Id_Cajero', $Id_Cajero);
 return ($this->db->delete('cajero')) ? true: false;
}
}
?>