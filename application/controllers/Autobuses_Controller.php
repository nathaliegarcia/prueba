<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class Autobuses_Controller extends CI_Controller
{
	
	public function __construct(){
	
		parent::__construct();
			$this->load->model('Autobuses_Model');	
	}

public function index(){
	$data=array('page_title'=>'autobuses','view'=>'Autobuses_View','data_view'=>array());
	$autobuses=$this->Autobuses_Model->autobuses();
	$data['autobuses']=$autobuses;
	$this->load->view('template/main',$data);
}
public function cronometro(){
	$data=array('page_title'=>'autobuses','view'=>'cronometro','data_view'=>array());
	$this->load->view('template/main',$data);
}

public function agregar_autobus(){
	$data=array('page_title'=>'','view'=>'Agregar_autobus_View','data_view'=>array());
	$caracteristicas=$this->Autobuses_Model->caracteristicas();
	$data['caracteristicas']=$caracteristicas;

	$parada_autobuses=$this->Autobuses_Model->parada_autobuses();
	$data['parada_autobuses']=$parada_autobuses;

	$horario=$this->Autobuses_Model->horario();
	$data['horario']=$horario;

	$mantenimiento=$this->Autobuses_Model->mantenimiento();
	$data['mantenimiento']=$mantenimiento;

	$this->load->view('template/main',$data);
}

public function agregar(){
	if($this->input->is_ajax_request()){
		$data= array(
			'capacidad' => $this->input->post('capacidad'),
			'idcaracteristicas' => $this->input->post('caracteristicas'),
			'idparada_autobuses' => $this->input->post('parada_autobuses'),
			'idhorario' => $this->input->post('horario'),
			'idmantenimiento' => $this->input->post('mantenimiento'),
			'combustible' => $this->input->post('combustible')
		);
		if($this->Autobuses_Model->agregar_autobus($data))
		{
			echo json_encode(array('success' => 1));
		}
		else
		{
			echo json_encode(array('success' => 0));
		}
	}
	else{

	echo "Nose puede acceder";

}


}
//delete

public function eliminar($idautobuses){
	$this->Autobuses_Model->eliminar($idautobuses);
	$this->index();
}

//update
public function Mostrar_Act(){

}

}

?>