<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller
{
	
	public function __construct()
	{
		parent:: __construct();

		$this->load->model('usuario');
	}
	public function CargarUsuario()
	{
		$data = array(
			'page_title' => 'USUARIOS',
			'view' => 'usuario',
			'data_view' => array()
		);
		$this->load->view('Template/main',$data);
	}
	public function FormUsuario()
	{
		$data = array(
			'page_title' => 'FORMULARIO DE USUARIOS',
			'view' => 'form/usuario',
			'data_view' => array()
		);
		$this->load->view('Template/main',$data);
	}
	public function InsertarUsuario(){

		if($this->input->is_ajax_request())
		{
			$data = array(
				'usuario' => $this->input->post('usuario'),
				'contraseña' => $this->input->post('contraseña'),
				'idrol' => $this->input->post('idrol');
			);
			if($this->usuario->AgregarUsuario())
			{
				echo json_encode(array('success' =>1));
			}
			else
			{
				echo json_encode(array('success' =>0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}
	}
	public function ActUsuario()
	{
		$data = array(
			'page_title' => 'ACTUALIZADOR DE USUARIOS',
			'view' => 'act/usuari',
			'data_view' => array()
		);
		$this->load->view('Template/main',$data);
	}
	public function UpdateUsuario()
	{
		if($this->input->is_ajax_request())
		{
			$data = array(
				'idusuario' => $this->input->post('idusuario'),
				'usuario' => $this->input->post('usuario'),
				'contraseña' => $this->input->post('contraseña'),
				'idrol' => $this->input->post('idrol');
			);
			if($this->usuario->ActualizarUsuario())
			{
				echo json_encode(array('success' =>1));
			}
			else
			{
				echo json_encode(array('success' =>0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}
	}
	public function DeleteUsuario($idusuario)
	{
		$this->usuario->EliminarUsuario($idusuario);
		$this->CargarUsuario();
	}
}
 ?>