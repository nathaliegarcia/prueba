<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 */
class control_autobuses_controller extends CI_Controller
{
	
	public function __construct(){
	
		parent::__construct();
			$this->load->model('control_autobuses_model');	
	}

	public function index(){
		$data=array('page_title'=>'control','view'=>'control_autobuses_view','data_view'=>array());
		$control=$this->control_autobuses_model->control();
		$data['control']=$control;
		$this->load->view('template/main',$data);
	}

	public function control(){
		$data=array('page_title'=>'','view'=>'agregar_control','data_view'=>array());
		$horario=$this->control_autobuses_model->horario();
	     $data['horario']=$horario;
		$this->load->view('template/main',$data);
	}

	public function agregar_control(){

		if($this->input->is_ajax_request()){
			$data = array(
				'control_autobuses' => $this->input->post('control_autobuses'),
				'n_viajeros' => $this->input->post('n_viajeros'),
				'idhorario' => $this->input->post('horario')
			);
			if($this->control_autobuses_model->agregar_control($data))
			{
				echo json_encode(array('success' => 1));
			}
			else
			{
				echo json_encode(array('success' => 0));
			}
		}
		else
		{
			echo "No se puede acceder";
		}
	}

	//////////////////////////////////

	public function eliminar($idcontrol_autobuses){
		$this->control_autobuses_model->eliminar($idcontrol_autobuses);
		$this->index();
	}

//////update de control

	public function update_control(){
		$data=array('page_title'=>'ACT','view'=>'act_control','data_view'=>array());
		$horario=$this->control_autobuses_model->horario();
	     $data['horario']=$horario;
		$this->load->view('template/main',$data);
	}


	public function actualizar_control(){
		if($this->input->is_ajax_request()){
			$data = array(
				'control_autobuses' =>$this->input->post('control_autobuses'),
				'n_viajeros' =>$this->input->post('n_viajeros'),
				'idhorario' => $this->input->post('horario')
			);
			if($this->control_autobuses_model->actualizar_control($data))
			{
				echo json_encode(array('success' => 1));
			}
			else
			{
				echo json_encode(array('success' => 0));
			}
		}
		else
		{
			echo "No se pudo acceder";

		}

	}

}
	?>