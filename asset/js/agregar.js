$('#agregar').click(function(){
    var capacidad = $('#capacidad').val(),
        caracteristicas = $('#caracteristicas').val(),
        parada_autobuses = $('#parada_autobuses').val(),
        horario = $('#horario').val(),
        mantenimiento = $('#mantenimiento').val(),
        combustible = $('#combustible').val();

        $.ajax({

            dataType:'json',
            url:baseurl+'Autobuses_Controller/agregar',
            type: 'post',
            dataType: 'json',
            data:{

                capacidad, caracteristicas, parada_autobuses, horario, mantenimiento, combustible
            },

            dataType: 'json',

            beforesend: function(){

            },

            success: function(data){
                if(data.success === 1){
                    alert("Registro insertados");

                    document.location.href=baseurl+'Autobuses_Controller/';
                }else{
                    alert("No se pudo ingresar el registro");
                }
            },

            error: function(e){
                alert("Algo fallo, consulte al administrador");
                console.log(e);
            }
        });

});