$('#insertar').click(function () {
	var control_autobuses = $('#control').val(),
	    n_viajeros = $('#numero').val(),
	    horario = $('#horario').val();

	    $.ajax({
	    	dataType: 'json',
	    	url:baseurl+'Control_autobuses_controller/agregar_control',
	    	type: 'post',
	    	dataType: 'json',
	    	data:{
	    		control_autobuses, n_viajeros, horario
	    	},
	    	dataType:'json',

	    	beforesend: function(){

	    	},
	    	success:function(data){
	    		if(data.success === 1){
	    			alert("Registro insertado");
	    			document.location.href=baseurl+'Control_autobuses_controller';
	    		}else{
	    			alert("No se pudo ingresar el Registro");
	    		}
	    	},

	    	error:function(e){
	    		alert("Algo fallo, consulte con el administrador");
	    		console.log(e);
	    	}
	    });

});

$('#actualizar').click(function () {
	var idcontrol_autobuses = $('#idcontrol_autobuses').val(),
	    control_autobuses = $('#control').val(),
	    n_viajeros =$('#numero').val(),
	    horario = $('#horario').val();

	    $.ajax({
	    	dataType:'json',
	    	url:baseurl+'Control_autobuses_controller/actualizar_control',
	    	type: 'post',
	    	dataType: 'json',
	    	data:{
	    		control_autobuses, n_viajeros, horario
	    	},
	    	dataType: 'json',

	    	beforesend: function(){

	    	},
	    	success:function(data){
	    		if(data.success === 1){
	    			alert("Registro Actualizado");
	    			document.location.href=baseurl+'Control_autobuses_controller/';
	    		}else{
	    			alert("No se pudo actualizar el registro");
	    		}
	    	},

	    	error:function(e){
	    		alert("Algo fallo consulte con el administrador");
	    		console.log(e);
	    	}
	    });

});
